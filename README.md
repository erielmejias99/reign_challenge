# reign_challenge 2021/5/17

Steps to run:

Requirements **docker**, **docker-compose** and **5000, 5002, 5003** Ports not in Use.

1. Clone this repository
	> git clone https://gitlab.com/erielmejias99/reign_challenge.git

2. Move to the repository
	> cd reign_challenge/

3. Build and run all images
	> docker-compose up


When all the process finishes open your browser in **http://localhost:5002**.


If your have any doubts just contact me [erielmejias99@gmail.com](mailto:erielmejias99@gmail.com)
