import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MyheaderComponent } from './myheader/myheader.component';
import { MytableComponent } from './mytable/mytable.component';
import { ArticleComponent } from './article/article.component';
import { HttpClientModule } from '@angular/common/http';
import { MyfooterComponent } from './myfooter/myfooter.component';

@NgModule({
  declarations: [
    AppComponent,
    MyheaderComponent,
    MytableComponent,
    ArticleComponent,
    MyfooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
