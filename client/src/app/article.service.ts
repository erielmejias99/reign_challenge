import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { base_url } from "./config/apibackend";
import { Observable, of } from "rxjs";
import { Article } from "./article/article";
import axios, {AxiosResponse} from 'axios';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private url: string = '/api/v1/article';

  constructor( private http: HttpClient ) { }

  getArticles(): Promise<AxiosResponse<Article[]>>{
    return axios.get<Article[]>( base_url + this.url );
  }

  deleteArticle( article_id:string ): Observable<any>{
    return this.http.delete( base_url + this.url + '/' + article_id );
  }

}
