import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {Article} from "./article";
import {ArticleService} from "../article.service";
import * as moment from "moment";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  // Color used to Hover
  background_color:string = '#fff';

  @Input() article: Article;
  @Output() delete = new EventEmitter<Article>();

  constructor( private  articleService: ArticleService ) {
    this.article = {
      _id: '',
      article_id: 0,
      title: "",
      story_title: "",
      author: '',
      story_url: '',
      created_at: '',
      url:''
    };
  }

  // Take complete date and return Human readable date
  computeDate():string{
    let date = moment( this.article.created_at );
    let now = moment();

    // If the date of the article is today just show the Hour and Minutes
    if( now.isSame(date, 'date') ){
      return date.format("HH:MM A");
    }else if( date.isSame( moment().subtract(1, 'days').startOf('day'), 'd' )){
      // if the date is Yesterday just return Yesterday
      return 'Yesterday'
    }else if( date.year() == now.year() && date.month() === date.month()  ){
      // if the date is in the same month just show the "Month Date"
      return date.format( "MMM DD");
    }else if( date.year() == now.year() ){
      return date.format( "YYYY/M/D")
    }
    // return the complete date
    return date.toDate().toISOString();
  }

  openArticle():void{
    // Take the non null field url
    let url: string|undefined|null  = this.article.story_url == null ? this.article.url : this.article.story_url;
    if( url != undefined ){
      // redirect to a new tab
      window.open( url, '_blank' )
    }
  }

  deleteArticle( $event : Event ):void{
    // Stop event propagation without this line the article will open in a new tab on delete action.
    $event.stopPropagation();
    if( confirm( 'Are you sure you want to delete this article?') ){
      this.articleService.deleteArticle( this.article._id ).subscribe( resp=>{
        // Notify the Parent component to remove it
        this.delete.emit( this.article );
      }, ()=>{
        alert( "Error deleting the Article... :(")
      })
    }
  }

  ngOnInit(): void {
    // format the Date to Human Readable
    this.article.created_at = this.computeDate();
  }

}
