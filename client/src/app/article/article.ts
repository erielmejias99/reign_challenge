
export interface Article{
    _id: string;
    article_id: number;
    author: string;
    created_at: string;
    title?: string|null;
    story_title?: string|null;
    url?: string|null;
    story_url?: string|null;
}
