import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myheader',
  templateUrl: './myheader.component.html',
  styleUrls: ['./myheader.component.css']
})
export class MyheaderComponent implements OnInit {
  title= 'HN Feed';
  subtitle = 'We <3 hacker news!'

  constructor() {

  }

  ngOnInit(): void {

  }

}
