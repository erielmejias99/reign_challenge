import { Component, OnInit } from '@angular/core';
import {Article} from "../article/article";
import {ArticleService} from "../article.service";

@Component({
  selector: 'app-mytable',
  templateUrl: './mytable.component.html',
  styleUrls: ['./mytable.component.css']
})
export class MytableComponent implements OnInit {

  articles: Article[] = [];
  loading: boolean = false;

  constructor( private articleService: ArticleService ) {
    this.articles = [];
  }

  loadArticles(){
    this.loading = true;
    this.articleService.getArticles().then( articles => {
      for( let arctice of articles.data ){
        if( arctice.url != null || arctice.story_url != null ){
          this.articles.push( arctice )
        }
      }
      this.loading = false;
    }).catch( err=>{
      if( err.response ){
        console.log(err.response);
      }else{
        console.log( err );
      }
    });
  }

  selectArticle( article :Article){
    window.open( "eriel.com",  '_blank');
  }

  deleteArticle( article: Article ){
    // @ts-ignore
    const index: number = this.articles.indexOf( article );
    if( index != -1 ){
      this.articles.splice( index, 1 );
    }
  }

  ngOnInit(): void {
    this.loadArticles();
  }

}
