import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Article, ArticleSchema, ArticleDocument } from "./article.schema";
import { mongoDB_URI } from "./article.module";
import { Document } from "mongoose";

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports:[
        MongooseModule.forRoot(mongoDB_URI),
        MongooseModule.forFeature([
          {
            name: 'Article',
            schema: ArticleSchema,
            collection: 'article',
          },
        ]),
      ],
      controllers: [ArticleController],
      providers: [ArticleService],
    }).compile();
  });

  describe('getHello', () => {
    it('should return the same number of Artilces', async () => {
      const appController = app.get<ArticleController>(ArticleController);
      const articleService = app.get<ArticleService>(ArticleService);
      const articles: any[] = await appController.getArticles()
      const service_articles: any[] = await articleService.getArticles()
      expect( articles.length ).toBe( service_articles.length );
    });
  });

  describe( 'createNewArticle', ()=>{
    it( 'should create a new article in the database', async ()=>{
      const articleService = app.get<ArticleService>(ArticleService);

      let article: Article = {
        article_id: (new Date()).getTime(),
        author: 'erielmejias99',
        title: "Cryptocurrency ... the future..",
        story_title: null,
        story_url: null,
        url: 'https://google.com',
        created_at: new Date().toISOString(),
        deleted: false
      }
      // create the article
      let article_created = await articleService.createArticle( article );

      // check article is created
      expect( article_created ).toBeInstanceOf( Document );

    })
  })

  describe( 'deleteArticle', ()=>{
    it( 'should set the field deleted of an article to True', async ()=>{
      const appController = app.get<ArticleController>(ArticleController);
      const articleService = app.get<ArticleService>(ArticleService);

      let article: Article = {
        article_id: (new Date()).getTime(),
        author: 'erielmejias99',
        title: "Cryptocurrency ... the future..",
        story_title: null,
        story_url: null,
        url: 'https://google.com',
        created_at: new Date().toISOString(),
        deleted: false
      }
      // create the article
      let article_created = await articleService.createArticle( article );
      // const service_articles: any[] = await articleService.getArticles()
      // check article is created
      expect( article_created ).toBeInstanceOf( Document );

      if( article_created instanceof Document ){
        let article_created_doc = <ArticleDocument>article_created;
        let resp:ArticleDocument = await appController.delete( article_created_doc._id )
        expect( resp.delete ).toBeTruthy();
      }
    })
  })
});
