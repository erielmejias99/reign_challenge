import {
  Controller,
  Delete,
  Get,
  HttpCode, HttpException, HttpStatus,
  Param,
} from "@nestjs/common";

import { ArticleService } from './article.service';
import { Types } from "mongoose";

@Controller()
export class ArticleController {
  constructor( private readonly articleService: ArticleService ) {}

  @Get('/api/v1/article')
  async getArticles() {
    return this.articleService.getArticles();
  }

  @Delete('/api/v1/article/:id')
  @HttpCode(204 )
  async delete( @Param( 'id' ) id: string ) {
      if( Types.ObjectId.isValid( id ) ){
        //if Object ID is valid proceed to "DELETED" ( SoftDelete )
        return this.articleService.deleteArticle(id);
      }else{
        // if Object ID is not valid return BadRequest
        throw new HttpException(
          {
            code: "invalid_object_id",
            message: 'Object ID received is invalid.',
          },
          HttpStatus.BAD_REQUEST
        )
      }

  }
}
