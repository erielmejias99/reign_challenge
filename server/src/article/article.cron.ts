import { Injectable, Logger, OnModuleInit } from "@nestjs/common";
import { ArticleService } from "./article.service";
import { Cron, CronExpression, SchedulerRegistry } from "@nestjs/schedule";
import axios from "axios";
import { Article } from "./article.schema";

@Injectable()
export class UpdateArticlesTask implements OnModuleInit {

  private readonly logger = new Logger(UpdateArticlesTask.name);

  constructor(private articleService: ArticleService) {}

  async onModuleInit(): Promise<void> {
    // run the cron for the first time when server start.
    await this.retrieveNewArticles();
  }

  // Retrieve articles form the External API and save the new ones on the DataBase
  // @Cron( CronExpression.EVERY_10_SECONDS )
  @Cron(CronExpression.EVERY_HOUR )
  async retrieveNewArticles() {
    this.logger.debug("Load new Articles");
    try {
      // Retrieve articles form the API
      const ext_articles = await axios
        .get<any>("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");

      for (let article of ext_articles.data.hits) {

        // Do not add Articles with no URL
        if (article.url == null && article.story_url == null) {
          this.logger.debug("Article has no URL( url | story_url )");
          continue;
        }

        let new_article: Article = {
          article_id: article.objectID,
          author: article.author,
          title: article.title,
          story_title: article.story_title,
          story_url: article.story_url,
          url: article.url,
          created_at: new Date(article.created_at),
          deleted: false
        };
        try {
          //Try to save Article in DB
          await this.articleService.createArticle(new_article);
          this.logger.debug("Article created or Already exists...");
        } catch (err: any | Error) {
          this.logger.error("Article not saved... " + err.name + ": " + err.message);
        }
      }
    } catch (err: any | Error) {
      this.logger.error("Error fetching new articles... :( " + err.message);
    }
  }
}