export interface ArticleI {
  article_id: number
  author: string;
  created_at: Date;
  title?: string | null;
  story_title?: string | null;
  url?: string | null;
  story_url?: string | null;
  deleted: boolean
}
