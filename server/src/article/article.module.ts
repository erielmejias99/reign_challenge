import { Module } from '@nestjs/common';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { ArticleSchema } from './article.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { UpdateArticlesTask } from "./article.cron";

export const mongoDB_URI = 'mongodb://root:root@reigndatabase:27017/reign_database?authSource=admin';

@Module({
  imports: [
    MongooseModule.forRoot(mongoDB_URI),
    MongooseModule.forFeature([
      {
        name: 'Article',
        schema: ArticleSchema,
        collection: 'article',
      },
    ]),
    ScheduleModule.forRoot(),
  ],
  controllers: [ArticleController],
  providers: [ArticleService, UpdateArticlesTask ],
})
export class ArticleModule {}
