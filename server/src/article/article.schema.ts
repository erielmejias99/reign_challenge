import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from "mongoose";

@Schema()
export class Article{
  @Prop()
  article_id: number;

  @Prop()
  title: string;

  @Prop()
  story_title: string | null;

  @Prop()
  author: string | null;

  @Prop()
  url: string | null;

  @Prop()
  story_url: string | null;

  @Prop()
  created_at: Date;

  @Prop()
  deleted: boolean;
}

export type ArticleDocument = Article & Document
export const ArticleSchema = SchemaFactory.createForClass(Article);
