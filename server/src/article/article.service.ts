import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Article, ArticleDocument } from "./article.schema";
import { InjectModel } from "@nestjs/mongoose";
import { CastError, Model } from "mongoose";
import { notUpdated, unknownError } from "../util.common.errors";

@Injectable()
export class ArticleService {
  constructor(@InjectModel('Article') private readonly article: Model<ArticleDocument>) {}

  async getArticles():Promise<Article[]>{
    try{
      return await this.article
        .find({ deleted: false }).sort({ date: 1 } ).exec();
    }catch (any){
      throw new HttpException( unknownError, HttpStatus.INTERNAL_SERVER_ERROR );
    }
  }

  async deleteArticle(id: string): Promise<ArticleDocument> {
    try{

      // try to update object in DataBase
      var resp = await this.article.findByIdAndUpdate( id, {
        deleted: true,
      }, {
        new: true,
        // returnOriginal: false,
        rawResult: true
      });

    }catch (error: any|CastError ){
      // In case of any error occurred return BadRequest to user.
      throw new HttpException( unknownError, HttpStatus.BAD_REQUEST )
    }

    // Check if Article is updated or not
    if( resp.lastErrorObject.updatedExisting ){
      // Return the deleted Article
      return resp.value;
    }else{
      // If not updated ( Possible object not found ) return bad request
       throw new HttpException( notUpdated, HttpStatus.BAD_REQUEST );
    }

  }

  async createArticle( article: Article ): Promise<ArticleDocument|Article>{
      try{
        // Check if article is already in the database
        let exists_article: boolean = await this.article.exists( { article_id: article.article_id } )
        if( !exists_article ){
          return await this.article.create( article );
        }else{
          return article;
        }
      }catch ( err: any ){
        throw new HttpException( unknownError, HttpStatus.BAD_REQUEST );
      }
  }
}
