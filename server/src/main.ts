import { NestFactory } from '@nestjs/core';
import { ArticleModule } from './article/article.module';

async function bootstrap() {
  const app = await NestFactory.create(ArticleModule);
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
