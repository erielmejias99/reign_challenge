
export class ErrorResponse{
  code: string;
  message: string | null;
  error_code?: number | null;
}

export const unknownError: ErrorResponse = {
  code: 'unknown_error',
  message: 'Something is wrong.'
}

export const notUpdated: ErrorResponse = {
  code: 'object_not_updated',
  message: "Maybe an Object with object id doesn't exists."
}

