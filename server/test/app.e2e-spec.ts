import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { ArticleModule, mongoDB_URI } from "../src/article/article.module";
import { INestApplication } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { ArticleSchema } from "../src/article/article.schema";
import { ArticleController } from "../src/article/article.controller";
import { ArticleService } from "../src/article/article.service";

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports:[
        ArticleModule,
      ],
      controllers: [ArticleController],
      providers: [ArticleService],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe( 'fd', ()=>{
    it('/ (GET)', () => {
      let article = app.get<ArticleController>( ArticleController );
      return request(app.getHttpServer())
        .get('/')
        .expect(200)
        .expect( article.getArticles() );
    });
  })
});
